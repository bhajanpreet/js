```
// declaring some variables
// loosely typed language

var i = "some value";

let n = 34;

console.log(i, n);

/*
Variables, Operators, Arithmetic, 
Data Types
Functions, Objects, Events, Strings
Methods, Arrays, Loops, switch
JS Object
*/
let n1 = 43;

console.log(n + n1, n - n1, n * n1, n / n1, 2 ** 3);

console.log(Math.pow(2, 3));

console.log(Math.floor(n / n1));

var one = "1";
var two = 2;
console.log("----->", typeof true);

// String, Number, Boolean, null, undefined, Symbol

function doSomething(a, b) {
  return a + b;
}
var c = doSomething(2, 3);
console.log(c);

let obj = {
  name: "Bhajanpreet",
  age: 25
};

let obj1 = Object.create(obj);
obj1.brother = "Jaspreet";

let obj2 = Object.create(obj);

console.log(obj, obj1);

obj2.__proto__.brother = "Bro";

obj2.name = "Jassi";

console.log(obj2);
class Employee {
  constructor(n, a) {
    this.name = n;
    this.age = a;
  }
}

function EmployeeOne(n, a) {
  this.name = n;
  this.age = a;
}

let obj3 = new Employee("b", 12);

let obj4 = new Employee();

let obj5 = new EmployeeOne("b", 23);

console.log(obj3, obj4, obj5);

```


```
console.log(document.getElementById("txt_name").value);

var counter = 0;

function doSomething() {
  console.log("You're, ", document.getElementById("txt_name").value, ++counter);
}
```

